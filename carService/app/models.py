from django.db import models
# from djangotoolbox.fields import EmbeddedModelField
# from djongo import models


class Employee(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=500)
    phone = models.CharField(max_length=50)
    mail = models.EmailField(max_length=500)

class Client(models.Model):
    id = models.AutoField( primary_key=True)
    name = models.CharField(max_length=500)
    phone = models.CharField(max_length=50)
    mail = models.CharField(max_length=500)

class CarModel(models.Model):
    id = models.AutoField(primary_key=True)
    modelName = models.CharField(max_length=500)
    carBrand = models.CharField(max_length=500)

class CarEngine(models.Model):
    id = models.AutoField(primary_key=True)
    capacity = models.FloatField()
    fuelType = models.CharField(max_length=500)

PurchaseInvoice = {
    "date":"",
    "price":"",
    "employeeId":"",
    "customerId":""
}

class Car(models.Model):
    vin = models.CharField(primary_key=True, max_length=17)
    color = models.CharField(max_length=500)
    mileage = models.IntegerField()
    carModelId = models.ForeignKey("CarModel", on_delete=models.CASCADE)
    engineId = models.ForeignKey("CarEngine", on_delete=models.CASCADE)
    additionalInfo = models.CharField(max_length=1000, null=True)
    purchaseInvoice = models.JSONField(PurchaseInvoice)
    salesInvioce = models.JSONField(PurchaseInvoice)

