from rest_framework import serializers
from app.models import *
import json


class EmployeeSerializer(serializers.ModelSerializer):
    class Meta:
        model=Employee
        fields=('id', 'name', 'phone', 'mail')

class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model=Client
        fields= ('id', 'name', 'phone', 'mail')

class CarSerializer(serializers.ModelSerializer):
    class Meta:
        model=Car
        fields=('vin', 'color', 'mileage', 'carModelId', 'engineId', 'purchaseInvoice', 'salesInvioce', 'additionalInfo') 

class CarModelSerializer(serializers.ModelSerializer):
    class Meta:
        model=CarModel
        fields=('id', 'modelName', 'carBrand')

class CarEngineSerializer(serializers.ModelSerializer):
    class Meta:
        model=CarEngine
        fields=('id', 'capacity', 'fuelType')

