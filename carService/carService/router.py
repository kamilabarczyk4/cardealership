from app.views import *
from rest_framework import routers


router = routers.DefaultRouter()
router.register('employee', Employee)
router.register('client', Client)
router.register('carModel', CarModel)
router.register('carEngine', CarEngine)
router.register('car', Car)

