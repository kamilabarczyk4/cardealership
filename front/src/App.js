// import axios from 'axios';
// import React from 'react';

import React, { useState, useEffect, Component } from 'react';
import SplitPane from "react-split-pane";
import axios from 'axios';
import { BrowserRouter as Router } from 'react-router-dom'

import Car from './components/Car'
import Employee from './components/Employee'
import Client from './components/Client'
import CarModel from './components/CarModel'
import CarEngine from './components/CarEngine'
import Menu from './components/Menu'

var ScrollArea = require('react-scrollbar');

const myComponent = {
  width: '1400px',
  height: '1000px',
  overflowX: 'hidden',
  overflowY: 'scroll'
};

const elementComponent = {
  width: '1400px',
  height: '400px',
  overflowX: 'hidden',
  overflowY: 'scroll'
};


const RightSide = () => <div class="split right">
  <div class="right">

    <h2>Employee list</h2>
    <Employee />
    <Client />
    <CarModel />
    <CarEngine />
    <Car />
  </div>
</div>

export default function App() {
  return (
    // <RightSide />
    <Router>
      <div className="App">
        <div>
          <Menu />
        </div>
      </div>
    </Router>
  )
}
