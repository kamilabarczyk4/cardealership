// import React, { useState, useEffect } from 'react';
// import SplitPane from "react-split-pane";
// import axios from 'axios';

// // const useEmployeeList = () => {
// //   const [employees, setEmployees] = useState([]);

// //   useEffect(() => {
// //     // Your logic to fetch employee list goes here
// //     // Update the employees state with the fetched data
// //     setEmployees([...fetchedEmployees]);
// //   }, []);

// //   return employees;
// // };

// const employeesList = () => {
//     const [Employees, setEmployees] = useState([]);
//     const [newEmployee, setNewEmployee] = useState({
//         name: '',
//         phone: '',
//         mail: '',
//     });
//     const [updatedEmployee, setUpdateEmployee] = useState({
//         id: '',
//         make: '',
//         model: '',
//         year: '',
//     });

//     const fetchEmployees = async () => {
//         try {
//             const response = await axios.get('http://localhost:8000/employee');
//             setEmployees(response.data);
//         } catch (error) {
//             console.error('Error fetching employees:', error);
//         }
//     };
//     useEffect(() => {
//         fetchEmployees();
//     }, []);

//     //add employee
//     const handleInputChange = (e) => {
//         setNewEmployee({ ...newEmployee, [e.target.name]: e.target.value });
//     };

//     const handleUpdateChange = (e) => {
//         setUpdateEmployee({ ...updatedEmployee, [e.target.name]: e.target.value });
//     };

//     const handleAddEmployee = async () => {
//         try {
//             await axios.post('http://localhost:8000/employee/', newEmployee);
//             console.log('Employee added successfully!');
//             // Clear the input fields
//             setNewEmployee({ name: '', phone: '', mail: '' });
//             // Fetch the updated employee list
//             fetchEmployees();
//         } catch (error) {
//             console.error('Error adding employee:', error);
//         }
//     };

//     const handleDelete = async (employeeId) => {
//         try {
//             await axios.delete(`http://localhost:8000/employee/${employeeId}/`);
//             console.log('Employee deleted successfully!');
//             // Update the state to remove the deleted car from the table
//             setEmployees(Employees.filter((employee) => employee.id !== employeeId));
//         } catch (error) {
//             console.error('Error deleting car:', error);
//         }
//     };


//     const handleUpdateEmployee = async (employeeId) => {
//         try {
//             await axios.put(`http://localhost:8000/employee/${employeeId}/`, updatedEmployee);
//             console.log('Employee updated successfully!');
//             // Clear the input fields
//             setNewEmployee({ id: '', name: '', phone: '', mail: '' });
//             // Fetch the updated car list
//             fetchEmployees();
//         } catch (error) {
//             console.error('Error updating car:', error);
//         }
//     };

//     return (
//         <div>
//             <table className="employee-table">
//                 <thead>
//                     <tr>
//                         <th>Name</th>
//                         <th>Phone</th>
//                         <th>Mail</th>
//                     </tr>
//                 </thead>
//                 <tbody>
//                     {Employees.map((employee) => (
//                         <tr key={employee.id}>
//                             <td>{employee.name}</td>
//                             <td>{employee.phone}</td>
//                             <td>{employee.mail}</td>
//                             <td>
//                                 <button onClick={() => handleUpdateEmployee(employee.id)}>Update</button>
//                             </td>
//                             <td>
//                                 <button onClick={() => handleDelete(employee.id)}>Delete</button>
//                             </td>
//                         </tr>
//                     ))}
//                 </tbody>
//             </table>
//             <div>
//                 <h2>Update an employee</h2>
//                 <label>
//                     Name:
//                     <input type="text" name="name" value={updatedEmployee.name} onChange={handleUpdateChange} />
//                 </label>
//                 <label>
//                     Phone:
//                     <input type="text" name="phone" value={updatedEmployee.phone} onChange={handleUpdateChange} />
//                 </label>
//                 <label>
//                     Mail:
//                     <input type="text" name="mail" value={updatedEmployee.mail} onChange={handleUpdateChange} />
//                 </label>
//                 <h2>Add an employee</h2>
//                 <label>
//                     Name:
//                     <input type="text" name="name" value={newEmployee.name} onChange={handleInputChange} />
//                 </label>
//                 <label>
//                     Phone:
//                     <input type="text" name="phone" value={newEmployee.phone} onChange={handleInputChange} />
//                 </label>
//                 <label>
//                     Mail:
//                     <input type="text" name="mail" value={newEmployee.mail} onChange={handleInputChange} />
//                 </label>
//                 <button onClick={handleAddEmployee}>Add employee</button>
//             </div>
//         </div>

//     );
// }

// export default employeesList;