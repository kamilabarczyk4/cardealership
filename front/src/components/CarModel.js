import axios from 'axios';
import React, { useState } from 'react';
// import { Button, Form } from 'semantic-ui-react';
import DropDownList from 'react-dropdown-select';
import { useEffect } from 'react'
import { useFetcher } from 'react-router-dom';

const elementComponent = {
    position: 'absolute',
    top: '100px',
    left: '10px'
};

const cellComponent = {
    width: '100px',
    height: '20px'
};


export default function CarModel() {
    const [CarModel, setCarModels] = useState([]);
    const [newCarModel, setNewCarModel] = useState({
        modelName: '',
        carBrand: '',
    });
    const [updatedCarModel, setUpdateCarModel] = useState({
        id: '',
        modelName: '',
        carBrand: '',
    });

    const fetchCarModels = async () => {
        try {
            const response = await axios.get('http://localhost:8000/carModel/');
            setCarModels(response.data);
        } catch (error) {
            console.error('Error fetching CarModel:', error);
        }
    };

    useEffect(() => {
        fetchCarModels();
    }, []);

    const handleCarModelInputChange = (e) => {
        setNewCarModel({ ...newCarModel, [e.target.name]: e.target.value });
    };

    const handleCarModelUpdateChange = (e) => {
        setUpdateCarModel({ ...updatedCarModel, [e.target.name]: e.target.value });
    };

    const handleAddCarModel = async () => {
        try {
            await axios.post('http://localhost:8000/carModel/', newCarModel);
            console.log('Car Model added successfully!');
            // Clear the input fields
            setNewCarModel({ modelName: '', carBrand: '' });
            // Fetch the updated CarModel list
            fetchCarModels();
        } catch (error) {
            console.error('Error adding Car Model:', error);
        }
    };

    const handleCarModelDelete = async (carModelId) => {
        try {
            await axios.delete(`http://localhost:8000/carModel/${carModelId}/`);
            console.log('CarModel deleted successfully!');
            // Update the state to remove the deleted car from the table
            setCarModels(CarModel.filter((carModel) => carModel.id !== carModelId));
        } catch (error) {
            console.error('Error deleting CarModel:', error);
        }
    };


    const handleUpdateCarModel = async (carModelId) => {
        try {
            await axios.put(`http://localhost:8000/carModel/${carModelId}/`, updatedCarModel);
            console.log('CarModel updated successfully!');
            // Clear the input fields
            setNewCarModel({ id: '', modelName: '', carBrand: '' });
            // Fetch the updated car list
            fetchCarModels();
        } catch (error) {
            console.error('Error updating CarModel:', error);
        }
    };

    return (
        <div style={elementComponent}>
            <div >
                <table className="employee-table">
                    <thead>
                        <tr>
                            <th>Model</th>
                            <th>Marka</th>
                        </tr>
                    </thead>
                    <tbody>
                        {CarModel.map((carModel) => (
                            <tr key={carModel.id}>
                                <td>{carModel.modelName}</td>
                                <td>{carModel.carBrand}</td>
                                <td>
                                    <button onClick={() => handleUpdateCarModel(carModel.id)}>Modyfikuj</button>
                                </td>
                                <td>
                                    <button onClick={() => handleCarModelDelete(carModel.id)}>Usuń</button>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
                <div>
                    <h2>Zmodyfikuj model</h2>
                    <label>
                        Model:
                        <input type="text" name="modelName" value={updatedCarModel.modelName} onChange={handleCarModelUpdateChange} />
                    </label>
                    <label>
                        Marka:
                        <input type="text" name="carBrand" value={updatedCarModel.carBrand} onChange={handleCarModelUpdateChange} />
                    </label>
                    <h2>Dodaj model</h2>
                    <label>
                        Model:
                        <input type="text" name="modelName" value={newCarModel.modelName} onChange={handleCarModelInputChange} />
                    </label>
                    <label>
                        Marka:
                        <input type="text" name="carBrand" value={newCarModel.carBrand} onChange={handleCarModelInputChange} />
                    </label>
                    <button onClick={handleAddCarModel}>Dodaj</button>
                </div>
            </div>
        </div>

    );
}
