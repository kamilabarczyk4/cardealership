import axios from 'axios';
import React, { useState } from 'react';
// import { Button, Form } from 'semantic-ui-react';
import DropDownList from 'react-dropdown-select';
import { useEffect } from 'react'
import { useFetcher } from 'react-router-dom';

const elementComponent = {
    position: 'absolute',
    top: '100px',
    left: '10px'
};
const cellComponent = {
    width: '100px',
    height: '20px'
};


export default function Client() {

    const [Clients, setClients] = useState([]);
    const [newClient, setNewClient] = useState({
        name: '',
        phone: '',
        mail: '',
    });
    const [updatedClient, setUpdateClient] = useState({
        id: '',
        name: '',
        phone: '',
        mail: '',
    });

    const fetchClients = async () => {
        try {
            const response = await axios.get('http://localhost:8000/client/');
            setClients(response.data);
        } catch (error) {
            console.error('Error fetching clients:', error);
        }
    };
    useEffect(() => {
        fetchClients();
    }, []);

    const handleClientInputChange = (e) => {
        setNewClient({ ...newClient, [e.target.name]: e.target.value });
    };

    const handleUpdateChange = (e) => {
        setUpdateClient({ ...updatedClient, [e.target.name]: e.target.value });
    };

    const handleAddClient = async () => {
        try {
            await axios.post('http://localhost:8000/client/', newClient);
            console.log('Client added successfully!');
            // Clear the input fields
            setNewClient({ name: '', phone: '', mail: '' });
            // Fetch the updated employee list
            fetchClients();
        } catch (error) {
            console.error('Error adding employee:', error);
        }
    };

    const handleClientDelete = async (clientId) => {
        try {
            await axios.delete(`http://localhost:8000/client/${clientId}/`);
            console.log('Client deleted successfully!');
            // Update the state to remove the deleted car from the table
            setClients(Clients.filter((client) => client.id !== clientId));
        } catch (error) {
            console.error('Error deleting Client:', error);
        }
    };


    const handleUpdateClient = async (clientId) => {

        const response = await axios.get(`http://localhost:8000/client/${clientId}/`);

        const updatedData = {
            id: clientId,
            name: updatedClient.name || response.data.name,
            phone: updatedClient.phone || response.data.phone,
            mail: updatedClient.mail || response.data.mail
        }
        try {
            await axios.put(`http://localhost:8000/client/${clientId}/`, updatedData);
            console.log('Client updated successfully!');
            fetchClients();
        } catch (error) {
            console.error('Error updating client:', error);
        }
    };

    return (
        <div style={elementComponent}>
            <div >
                <table className="employee-table">
                    <thead>
                        <tr>
                            <th>Imię i nazwisko</th>
                            <th>Telefon</th>
                            <th>Mail</th>
                        </tr>
                    </thead>
                    <tbody>
                        {Clients.map((client) => (
                            <tr key={client.id}>
                                <td>{client.name}</td>
                                <td>{client.phone}</td>
                                <td>{client.mail}</td>
                                <td>
                                    <button onClick={() => handleUpdateClient(client.id)}>Modyfikuj</button>
                                </td>
                                {/* <td>
                                    <button onClick={() => handleClientDelete(client.id)}>Usuń</button>
                                </td> */}
                            </tr>
                        ))}
                    </tbody>
                </table>
                <div>
                    <h2>Modyfikuj dane:</h2>
                    <label>
                        Imię i nazwisko:
                        <input type="text" name="name" value={updatedClient.name} onChange={handleUpdateChange} />
                    </label>
                    <label>
                        Telefon:
                        <input type="text" name="phone" value={updatedClient.phone} onChange={handleUpdateChange} />
                    </label>
                    <label>
                        Mail:
                        <input type="text" name="mail" value={updatedClient.mail} onChange={handleUpdateChange} />
                    </label>
                    <h2>Dodaj kontrachenta</h2>
                    <label>
                        Imię i nazwisko:
                        <input type="text" name="name" value={newClient.name} onChange={handleClientInputChange} />
                    </label>
                    <label>
                        Telefon:
                        <input type="text" name="phone" value={newClient.phone} onChange={handleClientInputChange} />
                    </label>
                    <label>
                        Mail:
                        <input type="text" name="mail" value={newClient.mail} onChange={handleClientInputChange} />
                    </label>
                    <button onClick={handleAddClient}>Dodaj</button>
                </div>
            </div>
        </div>

    );
}