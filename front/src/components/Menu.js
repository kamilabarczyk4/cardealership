import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import '../menu.css';
import Employee from './Employee';
import Client from './Client';
import CarModel from './CarModel';
import CarEngine from './CarEngine';
import Car from './Car';


const Menu = () => {
    const [showEmployeeForm, setEmployeeShowForm] = useState(false);
    const [showCarEngineForm, setCarEngineShowForm] = useState(false);
    const [showCarModelForm, setCarModelShowForm] = useState(false);
    const [showCarForm, setCarShowForm] = useState(false);
    const [showClientForm, setClientShowForm] = useState(false);

    const handleEmployeeButtonClick = () => {
        setEmployeeShowForm(true);
        setClientShowForm(false);
        setCarEngineShowForm(false);
        setCarShowForm(false);
        setCarModelShowForm(false);
    };
    const handleClientButtonClick = () => {
        setEmployeeShowForm(false);
        setClientShowForm(true);
        setCarEngineShowForm(false);
        setCarShowForm(false);
        setCarModelShowForm(false);
    };
    const handlCarModelButtonClick = () => {
        setEmployeeShowForm(false);
        setClientShowForm(false);
        setCarEngineShowForm(false);
        setCarShowForm(false);
        setCarModelShowForm(true);
    };
    const handleCarEngineButtonClick = () => {
        setEmployeeShowForm(false);
        setClientShowForm(false);
        setCarEngineShowForm(true);
        setCarShowForm(false);
        setCarModelShowForm(false);
    };
    const handleCarButtonClick = () => {
        setEmployeeShowForm(false);
        setClientShowForm(false);
        setCarEngineShowForm(false);
        setCarShowForm(true);
        setCarModelShowForm(false);
    };


    return (
        <div className="menu-container">
            <div className="menu-bar">
                <div className="menu-items">
                    <button className="menu-button" onClick={handleEmployeeButtonClick}>
                        <Link to="/employee/" className="menu-link">Pracownicy</Link>
                    </button>
                    <button className="menu-button" onClick={handleClientButtonClick}>
                        <Link to="/client/" className="menu-link">Klienci</Link>
                    </button>
                    <button className="menu-button" onClick={handlCarModelButtonClick}>
                        <Link to="/carModel/" className="menu-link">Model</Link>
                    </button>
                    <button className="menu-button" onClick={handleCarEngineButtonClick}>
                        <Link to="/carEngine/" className="menu-link">Silnik</Link>
                    </button>
                    <button className="menu-button" onClick={handleCarButtonClick}>
                        <Link to="/car/" className="menu-link">Samochód</Link>
                    </button>
                </div>
            </div>
            <div>
                {showEmployeeForm && <Employee />}
                {showClientForm && <Client />}
                {showCarEngineForm && <CarEngine />}
                {showCarModelForm && <CarModel />}
                {showCarForm && <Car />}
            </div>

        </div>
    );
};
export default Menu;