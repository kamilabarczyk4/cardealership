import axios from 'axios';
import React, { useState } from 'react';
import { useEffect } from 'react'


const elementComponent = {
    position: 'absolute',
    top: '100px',
    left: '10px'
};



export default function Employee() {

    const [Employees, setEmployees] = useState([]);
    const [newEmployee, setNewEmployee] = useState({
        name: '',
        phone: '',
        mail: '',
    });
    const [updatedEmployee, setUpdateEmployee] = useState({
        id: '',
        name: '',
        phone: '',
        mail: '',
    });

    const fetchEmployees = async () => {
        try {
            const response = await axios.get('http://localhost:8000/employee/');
            setEmployees(response.data);
        } catch (error) {
            console.error('Error fetching employees:', error);
        }
    };
    useEffect(() => {
        fetchEmployees();
    }, []);

    //add employee
    const handleInputChange = (e) => {
        setNewEmployee({ ...newEmployee, [e.target.name]: e.target.value });
    };

    const handleUpdateChange = (e) => {
        setUpdateEmployee({ ...updatedEmployee, [e.target.name]: e.target.value });
    };

    const handleAddEmployee = async () => {
        try {
            await axios.post('http://localhost:8000/employee/', newEmployee);
            console.log('Employee added successfully!');
            // Clear the input fields
            setNewEmployee({ name: '', phone: '', mail: '' });
            // Fetch the updated employee list
            fetchEmployees();
        } catch (error) {
            console.error('Error adding employee:', error);
        }
    };

    const handleDelete = async (employeeId) => {
        try {
            await axios.delete(`http://localhost:8000/employee/${employeeId}/`);
            console.log('Employee deleted successfully!');
            // Update the state to remove the deleted car from the table
            setEmployees(Employees.filter((employee) => employee.id !== employeeId));
        } catch (error) {
            console.error('Error deleting car:', error);
        }
    };


    const handleUpdateEmployee = async (employeeId) => {

        const response = await axios.get(`http://localhost:8000/employee/${employeeId}/`);

        const updatedData = {
            id: employeeId,
            name: updatedEmployee.name || response.data.name,
            phone: updatedEmployee.phone || response.data.phone,
            mail: updatedEmployee.mail || response.data.mail
        }
        try {
            await axios.put(`http://localhost:8000/employee/${employeeId}/`, updatedData);
            console.log('Employee updated successfully!');
            fetchEmployees();
        } catch (error) {
            console.error('Error updating car:', error);
        }
    };


    return (

        <div className="App">
            <div style={elementComponent}>
                <table className="employee-table">
                    <thead>
                        <tr>
                            <th>Imię i nazwisko</th>
                            <th>Telefon</th>
                            <th>Mail</th>
                        </tr>
                    </thead>
                    <tbody>
                        {Employees.map((employee) => (
                            <tr key={employee.id}>
                                <td>{employee.name}</td>
                                <td>{employee.phone}</td>
                                <td>{employee.mail}</td>
                                <td>
                                    <button onClick={() => handleUpdateEmployee(employee.id)}>Zmień</button>
                                </td>
                                {/* <td>
                                    <button onClick={() => handleDelete(employee.id)}>Usuń</button>
                                </td> */}
                            </tr>
                        ))}
                    </tbody>
                </table>
                <div>
                    <h2>Zmodyfikuj dane pracownika:</h2>
                    <label>
                        Imię i nazwisko:
                        <input type="text" name="name" value={updatedEmployee.name} onChange={handleUpdateChange} />
                    </label>
                    <label>
                        Telefon:
                        <input type="text" name="phone" value={updatedEmployee.phone} onChange={handleUpdateChange} />
                    </label>
                    <label>
                        Mail:
                        <input type="text" name="mail" value={updatedEmployee.mail} onChange={handleUpdateChange} />
                    </label>
                    <h2>Dodaj pracownika</h2>
                    <label>
                        Imię i nazwisko:
                        <input type="text" name="name" value={newEmployee.name} onChange={handleInputChange} />
                    </label>
                    <label>
                        Telefon:
                        <input type="text" name="phone" value={newEmployee.phone} onChange={handleInputChange} />
                    </label>
                    <label>
                        Mail:
                        <input type="text" name="mail" value={newEmployee.mail} onChange={handleInputChange} />
                    </label>
                    <button onClick={handleAddEmployee}>Dodaj</button>
                </div>
            </div>
        </div>

    );
}