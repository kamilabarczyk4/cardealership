import axios from 'axios';
import React, { useState } from 'react';
// import { Button, Form } from 'semantic-ui-react';
import DropDownList from 'react-dropdown-select';
import { useEffect } from 'react'
import { useFetcher } from 'react-router-dom';


const elementComponent = {
    position: 'absolute',
    top: '100px',
    left: '10px'
};

const cellComponent = {
    width: '100px',
    height: '20px'
};


export default function Car() {

    const [vin, setVin] = useState('');
    const [color, setColor] = useState('');
    const [mileage, setMileage] = useState('');
    const [carModelId, setCarModelId] = useState('');
    const [carModels, setCarModels] = useState([]);
    const [carModel, setCarModel] = useState([]);
    const [engineId, setEngineId] = useState('');
    const [engines, setEngines] = useState([]);
    const [engine, setEngine] = useState([]);
    var [additionalInfo, setAdditionalInfo] = useState('');
    const [Cars, setCars] = useState([]);

    const [employees, setEmployees] = useState([]);
    // const [engines, setEngines] = useState([]);
    const [customers, setCustomers] = useState([]);


    const [date, setDate] = useState('');
    const [price, setPrice] = useState('');
    const [employeeId, setEmployeeId] = useState('');
    const [customerId, setCustomerId] = useState('');

    const [dateSale, setDateSale] = useState('');
    const [priceSale, setPriceSale] = useState('');
    const [employeeIdSale, setEmployeeIdSale] = useState('');
    const [customerIdSale, setCustomerIdSale] = useState('');



    const invoice = {
        date: date,
        price: price,
        employeeId: employeeId,
        customerId: customerId,
    }
    const saleInvoice = {
        date: "",
        price: "",
        employeeId: "",
        customerId: "",
    }

    useEffect(() => {
        fetchCars();
        fetchCarModels();
        fetchCarModel();
        fetchEngines();
        fetchEngine();
        fetchEmployees();
        fetchCustomers();
    }, []);

    const postData = async () => {
        var jsonInvoice = JSON.stringify(invoice);
        var jsonSaleInvoice = JSON.stringify(saleInvoice);

        if (additionalInfo === "") {
            additionalInfo = "-";
        }

        try {
            await axios.post('http://127.0.0.1:8000/car/', {
                vin,
                color,
                mileage,
                carModelId,
                engineId,
                additionalInfo,
                purchaseInvoice: JSON.parse(jsonInvoice),
                salesInvioce: JSON.parse(jsonSaleInvoice)
            });
            console.log('Car added successfully!');
            // Fetch the updated car list
            fetchCars();
        } catch (error) {
            console.error('Error adding car:', error);
        }

        setVin('');
        setColor('');
        setMileage('');
        setAdditionalInfo('');
        setCarModelId('');
        setEngineId('');
        setDate('');
        setPrice('');
        setEmployeeId('');
        setCustomerId('');

    }


    const handleUpdateInvoice = async (vin) => {

        const response = await axios.get(`http://localhost:8000/car/${vin}/`);

        saleInvoice.date = dateSale;
        saleInvoice.price = priceSale;
        saleInvoice.employeeId = employeeIdSale;
        saleInvoice.customerId = customerIdSale;

        var jsonPurchaseInvoice = JSON.stringify(response.data.purchaseInvoice);
        var jsonSaleInvoice = JSON.stringify(saleInvoice);
        const sales = JSON.stringify(response.data.salesInvioce);

        if (response.data.salesInvioce.date !== "") {
            jsonSaleInvoice = sales;
        }

        const updatedData = {
            vin: response.data.vin,
            color: response.data.color,
            mileage: response.data.mileage,
            carModelId: response.data.carModelId,
            engineId: response.data.engineId,
            additionalInfo: response.data.additionalInfo,
            purchaseInvoice: JSON.parse(jsonPurchaseInvoice),
            salesInvioce: JSON.parse(jsonSaleInvoice)
        }

        try {
            await axios.put(`http://localhost:8000/car/${vin}/`, updatedData);
            console.log('Employee updated successfully!');
            fetchCars();
        } catch (error) {
            console.error('Error updating car:', error);
        }

        setDateSale('');
        setPriceSale('');
        setEmployeeIdSale('');
        setCustomerIdSale('');
    };



    const fetchCars = async () => {
        try {
            const response = await axios.get('http://localhost:8000/car/');
            setCars(response.data);
        } catch (error) {
            console.error('Error fetching cars:', error);
        }


    };

    const handleDelete = async (carId) => {
        try {
            await axios.delete(`http://localhost:8000/car/${carId}/`);
            console.log('Car deleted successfully!');
            // Update the state to remove the deleted car from the table
            setCars(Cars.filter((car) => car.vin !== carId));
        } catch (error) {
            console.error('Error deleting Car:', error);
        }
    };

    const fetchEngines = async () => {

        try {
            const response = await axios.get('http://localhost:8000/carEngine/');
            setEngines(response.data);
        } catch (error) {
            console.error('Error fetching CarEngine:', error);
        }
    };

    const fetchEmployees = async () => {

        try {
            const response = await axios.get('http://localhost:8000/employee/');
            setEmployees(response.data);
        } catch (error) {
            console.error('Error fetching CarEngine:', error);
        }
    };

    const fetchCustomers = async () => {

        try {
            const response = await axios.get('http://localhost:8000/client/');
            setCustomers(response.data);
        } catch (error) {
            console.error('Error fetching CarEngine:', error);
        }
    };


    const fetchCarModels = async () => {
        try {
            const response = await axios.get('http://localhost:8000/carModel/');
            setCarModels(response.data); // Aktualizuj stan carModels
        } catch (error) {
            console.error('Error fetching CarModels:', error);
        }
    };

    const fetchCarModel = async () => {

        try {
            const response = await axios.get('http://localhost:8000/carModel/');
            setCarModel(response.data);
        } catch (error) {
            console.error('Error fetching CarModel:', error);
        }
    };

    const fetchEngine = async () => {

        try {
            const response = await axios.get('http://localhost:8000/carEngine/');
            setEngine(response.data);
        } catch (error) {
            console.error('Error fetching CarEngine:', error);
        }
    };

    const handleUpdateCar = async (vin) => {

        const response = await axios.get(`http://localhost:8000/car/${vin}/`);
        var jsonPurchaseInvoice = JSON.stringify(response.data.purchaseInvoice);
        var jsonSaleInvoice = JSON.stringify(response.data.salesInvioce);

        const updatedData = {
            vin: vin,
            color: color || response.data.color,
            mileage: mileage || response.data.mileage,
            carModelId: response.data.carModelId,
            engineId: engineId || response.data.engineId,
            additionalInfo: additionalInfo || response.data.additionalInfo,
            purchaseInvoice: JSON.parse(jsonPurchaseInvoice),
            salesInvioce: JSON.parse(jsonSaleInvoice)
        }
        try {
            await axios.put(`http://localhost:8000/car/${vin}/`, updatedData);
            console.log('Car updated successfully!');
            fetchCars();
        } catch (error) {
            console.error('Error updating car:', error);
        }
    };



    return (
        <div style={elementComponent}>
            <div>
                <table className="employee-table">
                    <thead>
                        <tr>
                            <th>VIN</th>
                            <th>Kolor</th>
                            <th>Przebieg</th>
                            <th style={cellComponent}>Model samochodu</th>
                            <th style={cellComponent}>Rodzaj paliwa i pojemność silnika</th>
                            <th>Dodatkowe informacje</th>
                            <th>Faktura kupna samochodu</th>
                            <th>Faktura sprzedaży samochodu</th>
                        </tr>
                    </thead>
                    <tbody>
                        {Cars.map((car) => (
                            <tr key={car.vin}>
                                <td>{car.vin}</td>
                                <td>{car.color}</td>
                                <td>{car.mileage}</td>
                                <td>{carModel.map((e, id) => {
                                    if (e.id === car.carModelId) {
                                        return <div><tr style={cellComponent}>{e.carBrand} {e.modelName}</tr>

                                        </div>
                                    }
                                })}</td>
                                <td>{engine.map((e) => {
                                    if (e.id === car.engineId) {
                                        return <div><tr style={cellComponent}>{e.fuelType} {e.capacity}</tr>

                                        </div>
                                    }
                                })}</td>
                                <td>{car.additionalInfo}</td>
                                <td>
                                    <tr>
                                        data: {car.purchaseInvoice.date}
                                    </tr>
                                    <tr>
                                        cena: {car.purchaseInvoice.price}
                                    </tr>
                                    <tr>
                                        Kupujący: {employees.map((e) => {
                                            if (e.id.toString() === car.purchaseInvoice.employeeId) {
                                                return e.name
                                            }
                                        })}
                                    </tr>
                                    <tr>
                                        Sprzedający: {customers.map((e) => {
                                            if (e.id.toString() === car.purchaseInvoice.customerId) {
                                                return e.name
                                            }
                                        })}
                                    </tr>

                                </td>
                                <td>
                                    <tr>
                                        data: {car.salesInvioce.date}
                                    </tr>
                                    <tr>
                                        cena: {car.salesInvioce.price}
                                    </tr>
                                    <tr>
                                        Sprzedający: {employees.map((e) => {
                                            if (e.id.toString() === car.salesInvioce.employeeId) {
                                                return e.name
                                            }
                                        })}
                                    </tr>
                                    <tr>
                                        Kupujący: {customers.map((e) => {
                                            if (e.id.toString() === car.salesInvioce.customerId) {
                                                return e.name
                                            }
                                        })}
                                    </tr>

                                </td>
                                <td>
                                    <button onClick={() => handleUpdateCar(car.vin)}>Modyfikuj dane</button>
                                </td>
                                <td>
                                    <button onClick={() => handleUpdateInvoice(car.vin)}>Dodaj fakturę sprzedaży</button>
                                </td>
                                <td>
                                    <button onClick={() => handleDelete(car.vin)}>Usuń</button>
                                </td>
                            </tr>
                        ))}

                    </tbody>
                </table>
                {/* <div> */}

            </div>
            <label>VIN:</label>
            <input
                placeholder="vin"
                value={vin}
                onChange={(e) => setVin(e.target.value)}
            />
            <label>Kolor:</label>
            <input
                placeholder="color"
                value={color}
                onChange={(e) => setColor(e.target.value)}
            />
            <label>Przebieg:</label>
            <input
                placeholder="mileage"
                value={mileage}
                onChange={(e) => setMileage(e.target.value)}
            />
            <label>Wybierz model samochodu:</label>
            <select value={carModelId} onChange={(e) => setCarModelId(e.target.value)}>
                <option value="">Wybierz model</option>
                {carModels.map(carModel => (
                    <option key={carModel.id} value={carModel.id}>
                        {carModel.carBrand} {carModel.modelName}
                    </option>
                ))}
            </select>
            <label>Wybierz rodzaj silnika:</label>
            <select value={engineId} onChange={(e) => setEngineId(e.target.value)}>
                <option value="">Wybierz silnik</option>
                {engines.map(engine => (
                    <option key={engine.id} value={engine.id}>
                        {engine.capacity} {engine.fuelType}
                    </option>
                ))}
            </select>
            <label>Dodatkowe informacje:</label>
            <input
                placeholder="additionalInfo"
                value={additionalInfo}
                onChange={(e) => setAdditionalInfo(e.target.value)}
            />

            <label><h1>Faktura kupna:</h1></label>
            <label>data:</label>
            <input
                placeholder="date"
                value={date}
                onChange={(e) => setDate(e.target.value)}
            />
            <label>cena:</label>
            <input
                placeholder="price"
                value={price}
                onChange={(e) => setPrice(e.target.value)}
            />
            <label>Kupujący:</label>
            <select value={employeeId} onChange={(e) => setEmployeeId(e.target.value)}>
                <option value="">Wybierz pracownika</option>
                {employees.map(employee => (
                    <option key={employee.id} value={employee.id}>
                        {employee.name}
                    </option>
                ))}
            </select>
            <label>Sprzedający:</label>
            <select value={customerId} onChange={(e) => setCustomerId(e.target.value)}>
                <option value="">Wybierz kontrahenta</option>
                {customers.map(customer => (
                    <option key={customer.id} value={customer.id}>
                        {customer.name}
                    </option>
                ))}
            </select>
            <button onClick={postData} primary>
                Dodaj
            </button>

            <label><h1>Faktura sprzedaży:</h1></label>
            <label>data:</label>
            <input
                placeholder="date"
                value={dateSale}
                onChange={(e) => setDateSale(e.target.value)}
            />
            <label>cena:</label>
            <input
                placeholder="price"
                value={priceSale}
                onChange={(e) => setPriceSale(e.target.value)}
            />
            <label>Sprzedający:</label>
            <select value={employeeIdSale} onChange={(e) => setEmployeeIdSale(e.target.value)}>
                <option value="">Wybierz pracownika</option>
                {employees.map(employee => (
                    <option key={employee.id} value={employee.id}>
                        {employee.name}
                    </option>
                ))}
            </select>
            <label>Kupujący:</label>
            <select value={customerIdSale} onChange={(e) => setCustomerIdSale(e.target.value)}>
                <option value="">Wybierz kontrahenta</option>
                {customers.map(customer => (
                    <option key={customer.id} value={customer.id}>
                        {customer.name}
                    </option>
                ))}
            </select>
        </div >
    );
}