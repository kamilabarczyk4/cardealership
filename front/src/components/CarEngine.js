import axios from 'axios';
import React, { useState } from 'react';
// import { Button, Form } from 'semantic-ui-react';
import DropDownList from 'react-dropdown-select';
import { useEffect } from 'react'
import { useFetcher } from 'react-router-dom';

const elementComponent = {
    position: 'absolute',
    top: '100px',
    left: '10px'
};

const cellComponent = {
    width: '100px',
    height: '20px'
};


export default function CarEnginesList() {
    const [CarEngines, setCarEngines] = useState([]);
    const [newCarEngine, setNewCarEngine] = useState({
        capacity: '',
        fuelType: '',
    });
    const [updatedCarEngine, setUpdateCarEngine] = useState({
        id: '',
        capacity: '',
        fuelType: '',
    });

    const fetchCarEngines = async () => {
        try {
            const response = await axios.get('http://localhost:8000/carEngine/');
            setCarEngines(response.data);
        } catch (error) {
            console.error('Error fetching carEngines:', error);
        }
    };
    useEffect(() => {
        fetchCarEngines();
    }, []);

    //add carEngine
    const handleInputChange = (e) => {
        setNewCarEngine({ ...newCarEngine, [e.target.name]: e.target.value });
    };

    const handleUpdateChange = (e) => {
        setUpdateCarEngine({ ...updatedCarEngine, [e.target.name]: e.target.value });
    };

    const handleAddCarEngine = async () => {
        try {
            await axios.post('http://localhost:8000/carEngine/', newCarEngine);
            console.log('CarEngine added successfully!');
            // Clear the input fields
            setNewCarEngine({ capacity: '', fuelType: '' });
            // Fetch the updated carEngine list
            fetchCarEngines();
        } catch (error) {
            console.error('Error adding carEngine:', error);
        }
    };

    const handleDelete = async (carEngineId) => {
        try {
            await axios.delete(`http://localhost:8000/carEngine/${carEngineId}/`);
            console.log('CarEngine deleted successfully!');
            // Update the state to remove the deleted car from the table
            setCarEngines(CarEngines.filter((carEngine) => carEngine.id !== carEngineId));
        } catch (error) {
            console.error('Error deleting car:', error);
        }
    };


    const handleUpdateCarEngine = async (carEngineId) => {
        try {
            await axios.put(`http://localhost:8000/carEngine/${carEngineId}/`, updatedCarEngine);
            console.log('CarEngine updated successfully!');
            // Clear the input fields
            setNewCarEngine({ id: '', capacity: '', fuelType: '' });
            // Fetch the updated car list
            fetchCarEngines();
        } catch (error) {
            console.error('Error updating car:', error);
        }
    };


    return (
        <div style={elementComponent}>
            <div>
                <table className="employee-table">
                    <thead>
                        <tr>
                            <th>Pojemność</th>
                            <th>Rodzaj paliwa</th>
                        </tr>
                    </thead>
                    <tbody>
                        {CarEngines.map((carEngine) => (
                            <tr key={carEngine.id}>
                                <td>{carEngine.capacity}</td>
                                <td>{carEngine.fuelType}</td>
                                <td>
                                    <button onClick={() => handleUpdateCarEngine(carEngine.id)}>Modyfikuj</button>
                                </td>
                                <td>
                                    <button onClick={() => handleDelete(carEngine.id)}>Usuń</button>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
                <div>
                    <h2>Zmodyfikuj:</h2>
                    <label>
                        Pojemność:
                        <input type="text" name="capacity" value={updatedCarEngine.capacity} onChange={handleUpdateChange} />
                    </label>
                    <label>
                        Rodzaj paliwa:
                        <input type="text" name="fuelType" value={updatedCarEngine.fuelType} onChange={handleUpdateChange} />
                    </label>
                    <h2>Dodaj silnik:</h2>
                    <label>
                        Pojwmność:
                        <input type="text" name="capacity" value={newCarEngine.capacity} onChange={handleInputChange} />
                    </label>
                    <label>
                        Rodzaj paliwa:
                        <input type="text" name="fuelType" value={newCarEngine.fuelType} onChange={handleInputChange} />
                    </label>
                    <button onClick={handleAddCarEngine}>Dodaj</button>
                </div>
            </div>
        </div>

    );
}